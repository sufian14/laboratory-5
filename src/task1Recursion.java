import java.util.Scanner;

public class task1Recursion {

	public static void main(String[] args) {
		// take the input number from user
		Scanner num = new Scanner(System.in);
		System.out.println("Enter a number:");
		int a = num.nextInt();

		System.out.printf("Factorial of " + a + " is: " + recursion(a));
	}

	// function uses recursion to calculate factorial of a number
	public static int recursion(int a) {
		if (a <= 1) return 1;
		else		return a * recursion(a - 1);
	}
}
