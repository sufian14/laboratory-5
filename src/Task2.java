import java.util.*;

public class Task2 {

	public static void main(String[] args) {
		// take an input word from user
		System.out.println("Enter a word to check if it's a palindrome:");
		Scanner word = new Scanner(System.in);
		String original = word.nextLine();

		// copy orginal string to a new stack
		Stack stack = new Stack();
		for (int i = 0; i < original.length(); i++)
			stack.push(original.charAt(i));

		// reverse order to a new string
		String reverse = "";
		while (!stack.isEmpty()) 
			reverse += stack.pop();

		if (reverse.equals(original))
			System.out.println("Yes! It's palindrome");
		else
			System.out.println("No! It isn't palindrome");
	}

}
