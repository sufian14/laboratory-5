import java.util.*;

public class task1Iteration {

	public static void main(String[] args) {
		//take the input number from user
		Scanner num = new Scanner(System.in);
		System.out.println("Enter a number:");
		int a = num.nextInt();
		
		System.out.printf("Factorial of " + a + " is: " + iteration(a));
	}
	
	//function uses iteration to calculate factorial of a number
	public static int iteration(int a) {
		int result = 1;
		for (int i = 1; i <= a; i++) {result *= i;}
		return result;
	}
}
